const weatherIcons = {
    "Rain" : "wi wi-day-rain",
    "Clouds" : "wi wi-day-cloudy",
    "Clear" : "wi wi-day-sunny",
    "Snow" : "wi wi-day-snow",
    "mist" : "wi wi-day-fog",
    "Drizzle" : "wi wi-day-sleet",
}

function capitalize(str) {

    return str[0].toUpperCase() + str.slice (1);

}

async function main(withGeo = true) {

    let geo;

    if(withGeo) {

    //récupération de la géolocalisation grâce a l'IP<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.10/css/weather-icons.min.css">
        geo = await fetch('http://ip-api.com/json')
            .then(resultat => resultat.json())
            .then(json => json.city)

    } else {
      
        geo = document.querySelector('#city').textContent;

  }

    //récupération de la météo grâce à la géalocalisation      
    const weather = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${geo}&appid=b109bc8ab7b73dd8e05b821d464aea3a
&lang=fr&units=metric`)
        .then(resultat => resultat.json())
        .then(json => json)

//Afficher les infos météo
displayWeatherInfos(weather)

}

function displayWeatherInfos(data) {
    const name = data.name;
    const temperature = data.main.temp;
    const conditions = data.weather[0].main;
    const description = data.weather[0].description;

    document.querySelector('#city').textContent = name;
    document.querySelector('#temperature').textContent = Math.round(temperature);
    document.querySelector('#conditions').textContent = capitalize(description);

    document.querySelector('i.wi').className = weatherIcons[conditions];

    document.body.className = conditions.toLowerCase();
}

const geo = document.querySelector('#city');

geo.addEventListener('click', () => {
    city.contentEditable = true;
});

geo.addEventListener('keydown', (e) => {
    if(e.keyCode === 13) {
        e.preventDefault();
        geo.contentEditable = false;
        main(false);
    }
})

main();